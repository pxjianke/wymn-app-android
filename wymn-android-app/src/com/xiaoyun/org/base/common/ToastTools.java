/**
 * 
 */
package com.xiaoyun.org.base.common;

import android.content.Context;
import android.widget.Toast;

/**
 * 封装的消息提示
 * 
 * @author yuanxy
 * 
 */
public class ToastTools {

	/**
	 * 弹出Toast消息
	 * 
	 * @param cont
	 *            上下文
	 * @param msg
	 *            内容
	 **/
	public static void ToastMessage(Context cont, String msg) {
		Toast.makeText(cont, msg, Toast.LENGTH_SHORT).show();
	}

	/***
	 * 弹出Toast消息
	 * 
	 * @param cont
	 *            上下文
	 * @param rId
	 *            String 对应的id
	 */
	public static void ToastMessage(Context cont, int rId) {
		String err = cont.getString(rId);
		ToastMessage(cont, err);
	}

	/**
	 * 自定义时间,弹出Toast消息
	 * 
	 * @param cont
	 *            上下文
	 * @param msg
	 *            内容
	 * @param length_short
	 *            时间
	 */
	public static void ToastMessage(Context cont, String msg, int length_short) {
		Toast.makeText(cont, msg, length_short).show();
	}

	/**
	 * 自定义时间,弹出Toast消息
	 * 
	 * @param cont
	 *            上下文
	 * @param rId
	 *            String 对应的id
	 * @param length_short
	 *            时间
	 */
	public static void ToastMessage(Context cont, int rId, int length_short) {
		String err = cont.getString(rId);
		ToastMessage(cont, err, length_short);
	}

}
